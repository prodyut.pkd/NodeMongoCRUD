// CRUD create read update delete

const { MongoClient, ObjectID } = require('mongodb')

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'task-manager'

MongoClient.connect(connectionURL, { useNewUrlParser: true }, (error, client) => {
    if (error) {
        return console.log('Unable to connect to database!')
    }

    const db = client.db(databaseName)
    
    // db.collection('users').findOne({ _id: new ObjectID("5c1113239cbfe605241f9071") }, (error, user) => {
    //     if (error) {
    //         return console.log('Unable to fetch')
    //     }

    //     console.log(user)
    // })

     db.collection('users').find({ age: 27 }).toArray((error, users) => {
         console.log(users)
     })
     db.collection('users').find({ age: 27 }).count((error, users) => {
        console.log(users)
    })
    /*db.collection('tasks').findOne({ _id: new ObjectID("5ede53445233a13c1c710764") }, (error, task) => {
        console.log(task)
    })

    db.collection('tasks').find({ completed: true }).toArray((error, tasks) => {
        console.log(tasks)
    })*/
})